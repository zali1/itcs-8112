package com.zaireali.ITCS8112.main;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class BufferedImageLoader {

	private BufferedImage image;
	
	public BufferedImage loadImage(String path) throws IOException{
		
		image = ImageIO.read(getClass().getResource(path));
		return image;
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
