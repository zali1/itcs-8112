package com.zaireali.ITCS8112.main;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Player {
	
	private double x;
	private double y;
	
	private double velX;
	private double velY;
	
	public static final int GRAVITY = 4;
	
	private BufferedImage player;
	
	public Player(double x, double y, Game game){
		
		this.x = x;
		this.y = y;
		
		
		
		BufferedImageLoader loader = new BufferedImageLoader();
		try{
			
			player = loader.loadImage("/lemon.png");
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void tick(){
		x+=velX;
		y+=velY;
		
		if(velY <= GRAVITY)
		{
			velY+=GRAVITY;
		}
		
		if(x <=0){
			x = 0;
		}
		if( x >= 640 - 18){
			x = 640 - 18;
		}
		if(y <=0){
			y = 0;
		}
		if( y >= 480 - 32){
			y = 480 - 32;
		}
		
		
	}
	
	public void render(Graphics g){
		g.drawImage(player, (int)x, (int)y, null);
		
		
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public void setX(double x){
		this.x = x;
	}
	
	public void setY(double y){
		this.y = y;
	}
	
	public void setVelX(double velX){
		this.velX = velX;
	}
	
	public void setVelY(double velY){
		this.velY = velY;
	}
	
	public double getVelX(){
		return velX;
	}
	
	public double getVelY(){
		return velY;
	}
	
	public double getWidth() {
		// TODO Auto-generated method stub
		return player.getWidth();
	}

	
	public double getHeight() {
		// TODO Auto-generated method stub
		return player.getHeight();
	}
	
	
	

}
