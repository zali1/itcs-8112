package com.zaireali.ITCS8112.main;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JFrame;

public class Game extends Canvas implements Runnable {

	
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 320;
	public static final int HEIGHT = WIDTH / 12 * 9;
	public static final int SCALE = 2;
	public final String TITLE = "The Munchies";
	
	private boolean running = false;
	
	private boolean is_shooting = false;
	
	private boolean is_boss = false;
	
	
	
	private Thread thread;

	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private BufferedImage spriteSheet = null;
	private BufferedImage background = null;
	
	//private BufferedImage player;
	private Player p;
	private Controller c;
	private Menu menu;
	private BossStage boss;
	
	public static enum STATE{
		MENU,
		GAME,
		BOSS
	};
	
	public static STATE State = STATE.MENU;
	
	
	public void init(){
		//requestFocus();
		BufferedImageLoader loader = new BufferedImageLoader();
		try{
			spriteSheet = loader.loadImage("/sprite_street.png");
			background = loader.loadImage("/bg2.jpg");
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
		//SpriteSheet ss = new SpriteSheet(spriteSheet);
		//player = ss.grabImage(1, 1, 32, 32);
		
		addKeyListener(new KeyInput(this));
		this.addMouseListener(new MouseInput());
		
		p = new Player(200, 200, this);
		c = new Controller();
		menu = new Menu();
		
	}
	
	
	private synchronized void start(){
		if (running)
			return;
		
		running = true;
		thread = new Thread(this);
		thread.start();
		
	}
	
	private synchronized void stop(){
		if (!running)
			return;
		
		running = false;
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(1);
		
		
	}
	
	public static void main(String args[]){
		Game game = new Game();
		
		game.setPreferredSize(new Dimension (WIDTH * SCALE, HEIGHT * SCALE));
		game.setMaximumSize(new Dimension (WIDTH * SCALE, HEIGHT * SCALE));
		game.setMinimumSize(new Dimension (WIDTH * SCALE, HEIGHT * SCALE));
		
		
		JFrame frame = new JFrame(game.TITLE);
		frame.add(game);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		game.start();		
	
	}

	@Override
	public void run() {
		init();
		long lastTime = System.nanoTime();
		final double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		int updates = 0;
		int frames = 0;
		long timer = System.currentTimeMillis();
		
		c.addEntity(new Goal(550, HEIGHT * SCALE - 100));
		
		
		while(running){
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			if (delta >= 1){
				tick();
				updates++;
				delta--;
			}
			render();
			frames++;
			
			if(System.currentTimeMillis() - timer > 1000){
				
				timer += 1000;
				System.out.println(updates + " Ticks, FPS " + frames);
				updates = 0;
				frames = 0;
			}
		
		}
		stop();
		
	}
	
	private void tick(){
		
		if(State == STATE.GAME){
		
		p.tick();
		c.tick((int)p.getX(), (int)p.getY(), (int)p.getWidth(), (int)p.getHeight());
		}
		else if(State == STATE.BOSS){
			
			boss.tick();
			p.tick();
			c.tick((int)boss.getX(), (int)boss.getY(), (int)boss.getWidth(), (int)boss.getHeight());
			
			
			
			if(c.bulletBoss((int)boss.getX(), (int)boss.getY(), (int)boss.getWidth(), (int)boss.getHeight()))
			{
				System.out.println("BB");
				boss.setHP(boss.getHP()-1);
			}
			
		}
	}
	
	private void render(){
		
		BufferStrategy bs = this.getBufferStrategy();
		
		if (bs == null)
		{
			createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		
		
		
		g.drawImage(background, 0, 0, null);
		
		
		if(State == STATE.GAME){
		
			p.render(g);
			c.render(g);
		}
		else if(State == STATE.MENU){
			
			menu.render(g);
		}
		else if(State == STATE.BOSS){
			if (!is_boss){
				is_boss = true;
				boss = new BossStage();
				p.setX(200);
				p.setY(200);
				c = new Controller();
			}
			p.render(g);
			boss.render(g);
			c.render(g);
		}
		
		//g.drawImage(player, 100, 100, this);
		
		g.dispose();
		bs.show();
		
		
	}
	
	public void keyPressed(KeyEvent e){
		int key = e.getKeyCode();
		
		if(State == STATE.GAME){
			
			if(key == KeyEvent.VK_RIGHT){
				p.setVelX(5);
			}
			else if(key == KeyEvent.VK_LEFT)
			{
				p.setVelX(-5);
			
			}
			else if(key == KeyEvent.VK_DOWN)
			{
				
				if (p.getY() < HEIGHT * SCALE - 32){
					p.setVelY(p.getVelY() + 10);
					
				}
			}
			else if(key == KeyEvent.VK_UP)
			{
				
				if (p.getY() >= HEIGHT * SCALE - 32){
					p.setVelY(-35);
					
				}
				
			}
			else if(key == KeyEvent.VK_SPACE)
			{
				
				if(!is_shooting){
					c.addEntity(new Bullet(p.getX(), p.getY(), this));
					is_shooting = true;
				}
				
			
			}
			
			else if(key == KeyEvent.VK_E)
			{
				c.addEntity(new Enemy(650, p.getY()));
			}
			else if(key == KeyEvent.VK_C)
			{
				c.addEntity(new Collectable(650, p.getY()));
			}
			else if(key == KeyEvent.VK_P)
			{
				c.addEntity(new Platform(650, p.getY()));
			}
		}
		else if(State == STATE.MENU){
			
			
		}
		else if (State == STATE.BOSS)
			if(key == KeyEvent.VK_RIGHT){
				p.setVelX(5);
			}
			else if(key == KeyEvent.VK_LEFT)
			{
				p.setVelX(-5);
			
			}
			else if(key == KeyEvent.VK_DOWN)
			{
				
				if (p.getY() < HEIGHT * SCALE - 32){
					p.setVelY(p.getVelY() + 10);
					
				}
			}
			else if(key == KeyEvent.VK_UP)
			{
				
				if (p.getY() >= HEIGHT * SCALE - 32){
					p.setVelY(-35);
					
				}
				
			}
			else if(key == KeyEvent.VK_SPACE)
			{
				
				if(!is_shooting){
					c.addEntity(new Bullet(p.getX(), p.getY(), this));
					is_shooting = true;
					
				}
				
			
			}
	}
	
	public void keyReleased(KeyEvent e){
		
		int key = e.getKeyCode();
		
		if(State == STATE.GAME){
			
			if(key == KeyEvent.VK_RIGHT){
				p.setVelX(0);
			}
			else if(key == KeyEvent.VK_LEFT)
			{
				p.setVelX(0);
			
			}
			else if(key == KeyEvent.VK_DOWN)
			{
				//p.setVelY(0);
			}
			else if(key == KeyEvent.VK_UP)
			{
				//p.setVelY(0);
				
			}
			else if(key == KeyEvent.VK_SPACE)
			{
				
				is_shooting = false;
			
			}
		}	
		else if(State == STATE.MENU){
			
		
		}
		else if(State == STATE.BOSS){
			if(key == KeyEvent.VK_RIGHT){
				p.setVelX(0);
			}
			else if(key == KeyEvent.VK_LEFT)
			{
				p.setVelX(0);
			
			}
			else if(key == KeyEvent.VK_DOWN)
			{
				//p.setVelY(0);
			}
			else if(key == KeyEvent.VK_UP)
			{
				//p.setVelY(0);
				
			}
			else if(key == KeyEvent.VK_SPACE)
			{
				
				is_shooting = false;
			
			}
		}
	}
	
	
	
	public BufferedImage getSpriteSheet(){
		return spriteSheet;
	}
	
}
