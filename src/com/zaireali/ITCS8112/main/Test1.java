package com.zaireali.ITCS8112.main;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;


public class Test1 {
	
	//Test Main Menu State

        
		@Test
        public void toAndFromMM() {
                 Game tester = new Game(); // Game is Called
                 
                 tester.State = Game.STATE.MENU;
                 assertEquals(Game.STATE.MENU, tester.State);
                 
                 tester.State = Game.STATE.GAME;
                 assertEquals(Game.STATE.GAME, tester.State);

                 tester.State = Game.STATE.MENU;
                 assertEquals(Game.STATE.MENU, tester.State);                
        }
		
		
		//Test Player
		
		@Test
        public void player() {			
			
			for(int z = 0; z<= 10; z++)
			{
				Random rand = new Random();
				int  x = rand.nextInt(1001);
				int  y = rand.nextInt(1001);
				Player tester = new Player(x, y, null); // Player is Called
				assertEquals(x, (int)tester.getX());
                assertEquals(y, (int)tester.getY());
			
			}
		}
		
		//Test Collect
		@Test
        public void collect() {			
			
			for(int z = 0; z<= 10; z++)
			{
				Random rand = new Random();
				int  x = rand.nextInt(1001);
				int  y = rand.nextInt(1001);
				Collectable tester = new Collectable(x, y); // Collectable is Called
				assertEquals(x, (int)tester.getX());
                assertEquals(y, (int)tester.getY());
			
			}
		}
		
		//Test Platform
		@Test
        public void platform() {			
			
			for(int z = 0; z<= 10; z++)
			{
				Random rand = new Random();
				int  x = rand.nextInt(1001);
				int  y = rand.nextInt(1001);
				Platform tester = new Platform(x, y); // Platform is Called
				assertEquals(x, (int)tester.getX());
                assertEquals(y, (int)tester.getY());
			
			}
		}
		
		//Test Enemy
		@Test
        public void enemy() {			
			
			for(int z = 0; z<= 10; z++)
			{
				Random rand = new Random();
				int  x = rand.nextInt(1001);
				int  y = rand.nextInt(1001);
				Enemy tester = new Enemy(x, y); // Enemy is Called
				assertEquals(x, (int)tester.getX());
                assertEquals(y, (int)tester.getY());
			
			}
		}
		
		//Test Boss State
		@Test
        public void boss() {			
			
			for(int z = 0; z<= 10; z++)
			{
				Random rand = new Random();
				int  x = rand.nextInt(1001);
				int  y = rand.nextInt(1001);
				Boss tester = new Boss(x, y, 0); // Boss is Called
				assertEquals(x, (int)tester.getX());
                assertEquals(y, (int)tester.getY());
			
			}
		}
		
		
		
        
}