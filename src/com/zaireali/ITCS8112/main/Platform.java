package com.zaireali.ITCS8112.main;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Platform implements Entity{
	
	private double x;
	private double y;
	
	BufferedImage image;

	public Platform(double x, double y){
		this.x = x;
		this.y = y;
		

		BufferedImageLoader loader = new BufferedImageLoader();
		try{
			
			image = loader.loadImage("/platform2.png");
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void tick(){
		x -= 5;
		
	}
	
	public void render(Graphics g){
		g.drawImage(image, (int)x, (int)y, null);
		
		
	}
	
	public double getX(){
		return x;
	}

	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return y;
	}

	@Override
	public double getWidth() {
		// TODO Auto-generated method stub
		return image.getWidth();
	}

	@Override
	public double getHeight() {
		// TODO Auto-generated method stub
		return image.getHeight();
	}

	@Override
	public boolean collide(int x, int y, int w, int h) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 0;
	}

	

}
