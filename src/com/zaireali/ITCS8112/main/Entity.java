package com.zaireali.ITCS8112.main;

import java.awt.Graphics;

public interface Entity {

	public void tick();
	public void render(Graphics g);
	
	public double getX();
	public double getY();
	
	public double getWidth();
	public double getHeight();
	
	public boolean collide(int x2, int y2, int w2, int h2);
	
	public int getType();
}
