package com.zaireali.ITCS8112.main;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Bullet implements Entity {
	
	private double x;
	private double y;
	
	BufferedImage image;
	
	public Bullet(double x, double y, Game game){
		this.x = x;
		this.y = y;
		

		BufferedImageLoader loader = new BufferedImageLoader();
		try{
			
			image = loader.loadImage("/plant-seed.png");
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void tick(){
		x += 10;
		
	}
	
	public void render(Graphics g){
		//g.fillRect((int)x, (int)y, image.getWidth(), image.getHeight());
		g.drawImage(image, (int)x, (int)y, null);
		
	}

	@Override
	public double getX(){
		return x;
	}

	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return y;
	}

	

	@Override
	public double getWidth() {
		// TODO Auto-generated method stub
		return image.getWidth();
	}

	@Override
	public double getHeight() {
		// TODO Auto-generated method stub
		return image.getHeight();
	}

	@Override
	public boolean collide(int x2, int y2, int w2, int h2) {
		// TODO Auto-generated method stub
		Rectangle r1 = new Rectangle((int)x, (int)y, image.getWidth(), image.getHeight());
		Rectangle r2 = new Rectangle(x2, y2, w2, h2);
		
		if(r1.contains(r2))
			return true;
		return false;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	

}
