package com.zaireali.ITCS8112.main;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Enemy implements Entity {
	
	private double x;
	private double y;
	
	BufferedImage image;

	public Enemy(double x, double y){
		this.x = x;
		this.y = y;
		

		BufferedImageLoader loader = new BufferedImageLoader();
		try{
			
			image = loader.loadImage("/pretzel.png");
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void tick(){
		x -= 5;
		
	}
	
	public void render(Graphics g){
		g.drawImage(image, (int)x, (int)y, null);
		
		
	}
	
	public double getX(){
		return x;
	}

	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return y;
	}

	@Override
	public double getWidth() {
		// TODO Auto-generated method stub
		return image.getWidth();
	}

	@Override
	public double getHeight() {
		// TODO Auto-generated method stub
		return image.getHeight();
	}

	@Override
	public boolean collide(int x2, int y2, int w2, int h2) {
		// TODO Auto-generated method stub
		Rectangle r1 = new Rectangle((int)x, (int)y, image.getWidth(), image.getHeight());
		Rectangle r2 = new Rectangle(x2, y2, w2, h2);
		
		if(r1.contains(r2))
			return true;
		return false;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 3;
	}

	
	
	
}
