package com.zaireali.ITCS8112.main;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Boss{

	private double x;
	private double y;
	
	private int HP;
	
	private double velX;
	
	BufferedImage image;

	public Boss(double x, double y, int HP){
		this.x = x;
		this.y = y;
		this.HP = HP;
		this.velX = 0;

		BufferedImageLoader loader = new BufferedImageLoader();
		try{
			
			image = loader.loadImage("/boss.png");
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	public void tick() {
		// TODO Auto-generated method stub
		x+=velX;
		if (image.getWidth() + x >= Game.WIDTH * Game.SCALE)
		{
			velX = -2;
		}
		else if (x <= 0)
		{
			velX = 2;
		}
		
		
		
	}

	public void render(Graphics g) {
		// TODO Auto-generated method stub
		//g.fillRect((int)x, (int)y, image.getWidth(), image.getHeight());
		g.drawImage(image, (int)x, (int)y, null);
		
	}

	public double getX() {
		// TODO Auto-generated method stub
		return x;
	}

	public double getY() {
		// TODO Auto-generated method stub
		return y;
	}

	public double getWidth() {
		// TODO Auto-generated method stub
		return image.getWidth();
	}

	public double getHeight() {
		// TODO Auto-generated method stub
		return image.getHeight();
	}


	
	public int getHP() {
		// TODO Auto-generated method stub
		return HP;
	}
	public void setHP(int hp) {
		// TODO Auto-generated method stub
		HP = hp;
	}
}
