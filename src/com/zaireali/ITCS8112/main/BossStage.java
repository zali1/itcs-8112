package com.zaireali.ITCS8112.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.IOException;

public class BossStage {
	
	private Boss boss;
	
	public BossStage(){
		boss = new Boss(350, 250, 3);
		
	}


	
	public void render(Graphics g){
		boss.render(g);
		
		
	}
	
	public void tick(){
		boss.tick();
		if (boss.getHP() <= 0)
		{
			Game.State = Game.STATE.MENU;
		}
		
		
	}
	
	public double getX() {
		// TODO Auto-generated method stub
		return boss.getX();
	}

	
	public double getY() {
		// TODO Auto-generated method stub
		return boss.getY();
	}

	
	public double getWidth() {
		// TODO Auto-generated method stub
		return boss.getWidth();
	}

	
	public double getHeight() {
		// TODO Auto-generated method stub
		return boss.getHeight();
	}
	
	public int getHP() {
		// TODO Auto-generated method stub
		return boss.getHP();
	}
	public void setHP(int hp) {
		// TODO Auto-generated method stub
		boss.setHP(hp);
	}
}
