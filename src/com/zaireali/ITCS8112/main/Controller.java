package com.zaireali.ITCS8112.main;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

public class Controller {
	
	private LinkedList<Entity> e = new LinkedList<Entity>();
	
	Entity ent;
	
	
	
	public void tick(int x, int y, int w, int h){
		
		for(int i = 0; i < e.size(); i++){
			ent = e.get(i);
			ent.tick();
			if(ent.collide(x, y, w, h)){
				if (ent.getType() == 2) // Collectable
				{
					removeEntity(ent);
				}
				else if (ent.getType() == 3) // Enemy
				{
					removeEntity(ent);
					Game.State = Game.STATE.MENU;
				}
				else if (ent.getType() == 4) // Goal
				{
					removeEntity(ent);
					Game.State = Game.STATE.BOSS;
				}
				
			}
			if(ent.getX() >= Game.WIDTH * Game.SCALE && ent.getType() == 1){
				removeEntity(ent);
			}
			
		}
	}
	
	
	public boolean bulletBoss(int x2, int y2, int w2, int h2){
		for(int i = 0; i < e.size(); i++){
			ent = e.get(i);
			System.out.println("X: " + x2 + " X: " + ent.getX());
			
			Rectangle r1 = new Rectangle((int)ent.getX(), (int)ent.getY(), (int)ent.getWidth(), (int)ent.getHeight());
			Rectangle r2 = new Rectangle(x2, y2, w2, h2);
			
			//if(r1.contains(r2)){
			if(ent.getX() >= x2 && ent.getX() <= x2 + w2 && ent.getY() >= y2 && ent.getY() <= y2 + h2){
				removeEntity(ent);
				return true;
			}
		}
		return false;
	}
	
	public void render(Graphics g){
		for(int i = 0; i < e.size(); i++){
			ent = e.get(i);
			ent.render(g);
		}
	}
	
	public void addEntity(Entity block){
		e.add(block);
	}
	
	public void removeEntity(Entity block){
		e.remove(block);
	}
	

}
